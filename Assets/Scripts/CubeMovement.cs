﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    public event Action OnClossetPoint;

    [SerializeField] private float speed;
    [SerializeField] private ParticleSystem enemyEnterParticles;

    private bool _isCompleteMovement;
    private Sequence _sequence;

    private void Start()
    {
        _isCompleteMovement = true;
    }

    public void MoveToClosestEnemy(Transform target)
    {
        _sequence?.Kill();
        var rotatePoint = this.transform;
        rotatePoint.LookAt(target,Vector3.up);

        var duration = Vector3.Distance(target.position, transform.position) / speed;
        _sequence = DOTween.Sequence()
            .Append(transform.DOMove(target.position, duration))
            .Join(transform.DORotate(rotatePoint.eulerAngles,duration))
            .OnStart(()=> { _isCompleteMovement = false; })
            .OnComplete(()=> 
            {
                _isCompleteMovement = true;
                OnClossetPoint?.Invoke();
            });
    }

    public void PlayPS()
    {
        Instantiate(enemyEnterParticles, transform.position, Quaternion.identity);
    }

    public bool IsCompleteMovement => _isCompleteMovement;
}
