﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[Serializable, CreateAssetMenu(fileName = "EnemyPool", menuName = "Advance/EnemyPool", order = 0)]
public class EnemyPool : ScriptableObject
{
    private static EnemyPool _instance;

    public static EnemyPool Instance =>
        _instance ? _instance : _instance = Resources.Load<EnemyPool>(nameof(EnemyPool));

    public List<EnemyBehavior> enemyPool;
}