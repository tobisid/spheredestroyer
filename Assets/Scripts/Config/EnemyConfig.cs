﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class AllEnemies
{
    public List<EnemyConfig> enemies;
}

[Serializable]
public class EnemyConfig
{
    public EnemyType enemyType;
    public int reward;
    public int probability;
    public EnemyBehavior GetEnemy() 
    {
        return EnemyPool.Instance.enemyPool.FirstOrDefault(enemy => enemy.Type == enemyType);
    }
}