﻿using System;
using System.Collections;
using UnityEngine;

[Serializable]
public enum EnemyType 
{
    Weak,
    Middle,
    Strong
}
