﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnerManager : MonoBehaviour
{
    public event Action OnAddEnemy;
    public event Action<int> OnRemoveEnemy;

    [SerializeField] private Transform spawnHolder;
    [SerializeField] private float minSpawnDistance = 2f;

    private List<EnemyBehavior> _spawnedEnemies;
    private AllEnemies _enemyConfig;
    private Camera _camera;
    private Vector3 _viewport;

    private void Awake()
    {
        _spawnedEnemies = new List<EnemyBehavior>();
        _camera = Camera.main;
        _viewport = _camera.WorldToViewportPoint(Vector3.zero);
        StartCoroutine(LoadConfig());
    }

    private IEnumerator LoadConfig()
    {
        var jsonData = Resources.Load<TextAsset>("Enemies");
        _enemyConfig = JsonUtility.FromJson<AllEnemies>(jsonData.text);
        yield return null;
    }

    public void AddEnemy()
    {
        if(_spawnedEnemies == null)
        {
            _spawnedEnemies = new List<EnemyBehavior>();
        }

        var newEnemyConfig = GetRandomEnemy();
        var newEnemy = Instantiate(newEnemyConfig.GetEnemy(), spawnHolder);
        var point = RandomPosition();
        point.y = newEnemy.transform.position.y;
        newEnemy.transform.position = point;
        newEnemy.Reward = newEnemyConfig.reward;
        newEnemy.OnPlayerEnter += RemoveEnemy;

        _spawnedEnemies.Add(newEnemy);
        OnAddEnemy?.Invoke();
    }

    public void RemoveEnemy(EnemyBehavior enemy)
    {
        if (_spawnedEnemies.Contains(enemy))
        {
            _spawnedEnemies.Remove(enemy);
            Destroy(enemy.gameObject);
            OnRemoveEnemy?.Invoke(enemy.Reward);
        }
        else
        {
            Debug.Log("List don`t contains " + enemy.name);
        }
    }

    private EnemyConfig GetRandomEnemy()
    {
        var random = UnityEngine.Random.Range(0, 100);
        var enemyList = _enemyConfig.enemies;
        var chance = 0;
        var randomIndex = UnityEngine.Random.Range(0, enemyList.Count);
        foreach (var enemy in enemyList)
        {
            chance += enemy.probability;
            if(random <= chance)
            {
                return enemy;
            }
        }

        return enemyList[randomIndex];
    }

    private Vector3 RandomPosition()
    {
        var viewport = _viewport;
        viewport.x = UnityEngine.Random.Range(0.05f, .95f);
        viewport.y = UnityEngine.Random.Range(0.05f, .95f);
        var result = _camera.ViewportToWorldPoint(viewport);
        if(_spawnedEnemies == null || _spawnedEnemies.Count <= 1)
        {
            return result;
        }
        else
        {
            foreach (var enemy in _spawnedEnemies)
            {
                if(Vector3.Distance(enemy.transform.position , result) <= minSpawnDistance)
                {
                    result = RandomPosition();
                }
            }
            return result;
        }
    }

    public List<EnemyBehavior> SpawnedEnemies => _spawnedEnemies;
}
