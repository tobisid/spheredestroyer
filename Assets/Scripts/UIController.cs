﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public event Action OnAddButtonClick;

    [SerializeField] private TMP_Text scoreLabel;
    [SerializeField] private string scoreTemplate = "score: {0}";

    [SerializeField] private Button spawnButton;

    private void Start()
    {
        spawnButton.onClick.AddListener(SpawnButtonClickEvent);
    }

    public void UpdateScore(int value)
    {
        scoreLabel.text = string.Format(scoreTemplate, value);
    }

    private void SpawnButtonClickEvent()
    {
        OnAddButtonClick?.Invoke();
    }
}
