using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    public event Action<EnemyBehavior> OnPlayerEnter;
    [SerializeField] private EnemyType enemyType;
    private int _reward;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            OnPlayerEnter?.Invoke(this);
        }   
    }

    public int Reward 
    {
        get => _reward;
        set => _reward = value;
    }

    public EnemyType Type => enemyType;
}
