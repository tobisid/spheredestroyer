﻿using System.Collections;
using UnityEngine;


public class GameController : MonoBehaviour
{
    [SerializeField] private UIController uiController;
    [SerializeField] private CubeMovement cubeMovement;
    [SerializeField] private SpawnerManager spawnerManager;

    private int _score;
    void Start()
    {
        spawnerManager.OnAddEnemy += MoveToNext;
        spawnerManager.OnRemoveEnemy += PlayerRemoveEnemy;
        cubeMovement.OnClossetPoint += MoveToNext;
        uiController.OnAddButtonClick += AddEnemy;
    }

    private void MoveToNext()
    {
        if(spawnerManager.SpawnedEnemies == null || spawnerManager.SpawnedEnemies.Count == 0)
        {
            Debug.Log("Not enought enemies");
            return;
        }

        if(cubeMovement == null)
        {
            Debug.LogError("CubeMovement is null");
            return;
        }

        if (!cubeMovement.IsCompleteMovement)
        {
            return;
        }

        var dist = Mathf.Infinity;
        var index = 0;
        foreach (var enemy in spawnerManager.SpawnedEnemies)
        {
            if(Vector3.Distance(enemy.transform.position, cubeMovement.transform.position) < dist)
            {
                dist = Vector3.Distance(enemy.transform.position, cubeMovement.transform.position);
                index = spawnerManager.SpawnedEnemies.IndexOf(enemy);
            }
        }

        cubeMovement.MoveToClosestEnemy(spawnerManager.SpawnedEnemies[index].transform);
    }

    private void PlayerRemoveEnemy(int reward)
    {
        _score += reward;
        uiController.UpdateScore(_score);
        cubeMovement.PlayPS();
    }

    private void AddEnemy()
    {
        spawnerManager.AddEnemy();
    }
}
